package br.com.sicredi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class ClientRestApplication {
	public static void main(String[] args) {
		SpringApplication.run(ClientRestApplication.class, args);
	}
}

