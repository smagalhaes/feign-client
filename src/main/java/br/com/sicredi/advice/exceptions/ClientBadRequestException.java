package br.com.sicredi.advice.exceptions;

public class ClientBadRequestException extends RuntimeException{
	
	private static final long serialVersionUID = -3229743592278974771L;
	
	public ClientBadRequestException(String string) {
		super(string);
	}

	

}
