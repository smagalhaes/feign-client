package br.com.sicredi.advice;

import java.time.LocalDateTime;

public class ErrorDTO {
	private String message;
	private LocalDateTime when;
	
	public ErrorDTO(String message) {
		super();
		this.message = message;
		this.when = LocalDateTime.now();
	}
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public LocalDateTime getWhen() {
		return when;
	}
	public void setWhen(LocalDateTime when) {
		this.when = when;
	}
	
	
}
