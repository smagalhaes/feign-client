package br.com.sicredi.advice;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import br.com.sicredi.advice.exceptions.ClientBadRequestException;
import br.com.sicredi.service.AgenciaService;

@ControllerAdvice
public class AdviceController {
	

	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<Map<String, String>> methodArgumentReponse(MethodArgumentNotValidException ex) {
	    Map<String, String> errors = new HashMap<>();
	    ex.getBindingResult().getAllErrors().forEach(error -> {
	        String fieldName = ((FieldError) error).getField();
	        String errorMessage = error.getDefaultMessage();
	        errors.put(fieldName, errorMessage);
	    });
	    return ResponseEntity.badRequest().body(errors);
	}
	
	@ExceptionHandler(ClientBadRequestException.class)
	public ResponseEntity<ErrorDTO> entityNotFound(ClientBadRequestException ex) {
	    return ResponseEntity.badRequest().body(new ErrorDTO(ex.getMessage()));
	}
	
}
