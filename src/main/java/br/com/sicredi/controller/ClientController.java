package br.com.sicredi.controller;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import br.com.sicredi.dto.Agencia;
import br.com.sicredi.service.AgenciaService;

@RestController
public class ClientController {
	
	private AgenciaService agenciaService;
	
	public ClientController(AgenciaService agenciaService) {
		this.agenciaService = agenciaService;
	}

	@GetMapping
	public ResponseEntity<List<Agencia>> buscarTodos()
	{
		return ResponseEntity.ok(agenciaService.getAgencias());
	}
	
	
	@GetMapping("/{id}")
	public ResponseEntity<Agencia> buscarPorId(@PathVariable Long id)
	{
		return ResponseEntity.ok(agenciaService.findById(id));
	}
	
	
}
