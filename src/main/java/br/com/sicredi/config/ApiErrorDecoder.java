package br.com.sicredi.config;

import static br.com.sicredi.util.HttpStatusUtil.is4xxBadRequest;

import br.com.sicredi.advice.exceptions.ClientBadRequestException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class ApiErrorDecoder implements ErrorDecoder {

	@Override
	public Exception decode(String methodKey, Response response) {
		if(is4xxBadRequest(response.status()))
		{
			return new ClientBadRequestException("Não foi possível consumir a api por bad request");
		}
		
		return new Exception("Deu problema em tudo");
	}

}
