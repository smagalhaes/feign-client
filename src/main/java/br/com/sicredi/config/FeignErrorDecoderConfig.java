package br.com.sicredi.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import feign.codec.ErrorDecoder;

@Configuration
public class FeignErrorDecoderConfig {

	@Bean
	public ErrorDecoder errorDecoder() {
		return new ApiErrorDecoder();
	}
}
