package br.com.sicredi.client.agenciavirtual;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import br.com.sicredi.dto.Agencia;
import feign.Response;

@FeignClient(value="agenciaClient", url="http://localhost:8080/agencias")
public interface AgenciaRestClient {
	
	@GetMapping
	List<Agencia> getAgencias();
	
	@GetMapping("/{id}")
	Response findResponseById(@PathVariable Long id);
	
	@GetMapping("/{id}")
	Agencia findById(@PathVariable Long id);

}
