package br.com.sicredi.service;

import static br.com.sicredi.util.HttpStatusUtil.is2xxSuccessful;

import java.util.List;

import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.sicredi.client.agenciavirtual.AgenciaRestClient;
import br.com.sicredi.dto.Agencia;
import feign.Response;

@Service
public class AgenciaService {
	private AgenciaRestClient agenciaApiClient;
	private ObjectMapper objectMapper;

	public AgenciaService(AgenciaRestClient agenciaApiClient) {
		this.agenciaApiClient = agenciaApiClient;
		this.objectMapper = new ObjectMapper();
	}

	public List<Agencia> getAgencias(){
		return agenciaApiClient.getAgencias();
	}
	
	
	public Agencia findById(Long id) {
		if(id < 5) {
			return agenciaApiClient.findById(id);
		}
		
		return findResponseById(id);
		
	}
	
	public Agencia findResponseById(Long id) {
		Response agenciaResponse = agenciaApiClient.findResponseById(id);
		
		if(is2xxSuccessful(agenciaResponse.status())) {
			try {
				String content = IOUtils.toString(agenciaResponse.body().asReader());
				return objectMapper.readValue(content, Agencia.class);
			}catch (Exception e) {
				return null;
			}
		}
		
		return null;
	}

}
