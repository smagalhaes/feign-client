package br.com.sicredi.util;

public class HttpStatusUtil {

	public static boolean is2xxSuccessful(int httpStatus) {
		return httpStatus >= 200 && httpStatus < 300;
	}
	
	public static boolean is3xxRedirection(int httpStatus) {
		return httpStatus >= 300 && httpStatus < 400;
	}
	
	public static boolean is4xxBadRequest(int httpStatus) {
		return httpStatus >= 400 && httpStatus < 500;
	}
	
	public static boolean is5xxInternalServerError(int httpStatus) {
		return httpStatus >= 500 && httpStatus < 600;
	}
	
	public static boolean isCreated(int httpStatus) {
		return httpStatus == 201;
	}
}
