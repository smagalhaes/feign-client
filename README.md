# Objetivo:
Criar um exemplo de código para o uso do feing client.  

# Linguagem utilizada no desenvolvimento: 
JAVA  

# Deploy:  
O deploy em desenvolvimento é feito de forma automática ao realizar merge do código para branch master


# Instruções

Para rodar o projeto, navegue via terminal até a raiz do projeto e rode o comando  
**"java -jar agencia-virtual-0.0.1.jar"** isso 'levantará' uma api rest para consumo na porta **8080**  
acesse http://localhost:8080/swagger-ui.html para conferir a api.

Obs: Caso a aplicação não inicie por conta da porta, tente rodar o comando adicionando **--server.port=porta** ao final do comando